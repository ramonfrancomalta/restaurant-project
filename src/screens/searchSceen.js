//Libraries
import React, {useState} from 'react';
import {View, Text, StyleSheet,ScrollView} from 'react-native';
//Components
import SearchBar from '../components/SeachBar';
//APIs

//Hooks
import useResults from '../hooks/useResults';
import ResultList from '../components/ResultsList';

const SearchScreen = () => {
    const [term, setTerm] = useState('');  
    const [searchApi, results, errorMessage] = useResults();

    const filterResultByPrice = (price) => {
        return results.filter(result => {
            return result.price === price;
        });
    };

    return(   //<> cosa que nos menciono
        <View style={styles.background}>
            <SearchBar 
                term = {term} 
                onTermChange = {setTerm}
                onTermSubmit={() =>searchApi(term)}
            />
            {errorMessage? <Text>{errorMessage}</Text>: null}
            <ScrollView>
                <ResultList title = 'Cost Effective' results = {filterResultByPrice('$')}  />
                <ResultList title = 'Bit Pricier' results = {filterResultByPrice('$$')} />
                <ResultList title = 'Big Spender' results = {filterResultByPrice('$$$')} />
            </ScrollView>
        </View>
    )
};

const styles = StyleSheet.create({
    background:{
        backgroundColor: 'white',
        flex: 1
    }
});

export default SearchScreen;