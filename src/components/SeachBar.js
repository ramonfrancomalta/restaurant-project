import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import { Feather} from '@expo/vector-icons';

const SearchBar = ({term, onTermChange, onTermSubmit}) => {
    return(
        <View style={styles.backgroundStyle}>
            <Feather name = "search"  size= {30} style={styles.iconStyle} />
            <TextInput
                style={styles.inputStyle} 
                placeholder="Search" 
                value= {term}
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText = {onTermChange}
                onEndEditing={onTermSubmit}
            />
        </View>
    )
};

const styles = StyleSheet.create({ ///El tema con la configuacion dle padre es super importante porque literal es el que nos da que pauta seguir con los demas
    backgroundStyle: {
        backgroundColor: '#f0EEEE',
        height: 50,
        borderRadius: 5,
        marginLeft: 15,
        marginRight: 15,
        marginTop: 8,
        marginBottom: 5,
        flexDirection: 'row'
    },
    iconStyle:{
        fontSize: 35, //Tamaño del icono
        alignSelf: 'center',
        marginHorizontal: 8
    },
    inputStyle:{
        fontSize: 20,
        flex: 1
    }

});

export default SearchBar;