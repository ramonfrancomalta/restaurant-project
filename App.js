import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import SearchScreen from './src/screens/searchSceen';
import ResultsShowScreen from './src/screens/resultsShowScreen';

const navigator = createStackNavigator({
    Search: SearchScreen,
    ResultsShow: ResultsShowScreen
}, {
    initialRouteName: 'Search',
    defaultNavigationOptions: {
        title: 'BusinessSearch'
    }
});

export default createAppContainer(navigator);